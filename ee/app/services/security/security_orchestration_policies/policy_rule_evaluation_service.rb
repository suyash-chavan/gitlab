# frozen_string_literal: true

module Security
  module SecurityOrchestrationPolicies
    class PolicyRuleEvaluationService
      include Gitlab::Utils::StrongMemoize
      include ::Security::ScanResultPolicies::PolicyViolationCommentGenerator
      include ::Security::ScanResultPolicies::VulnerabilityStatesHelper

      def initialize(merge_request, approval_rules, report_type)
        @merge_request = merge_request
        @approval_rules = approval_rules
        @report_type = report_type
        @passed_rules = Set.new
        @failed_rules = Set.new
      end

      def save
        merge_request.reset_required_approvals(failed_rules) if failed_rules.any?
        ApprovalMergeRequestRule.remove_required_approved(passed_rules) if passed_rules.any?

        violations.execute

        generate_policy_bot_comment(
          merge_request,
          approval_rules.applicable_to_branch(merge_request.target_branch),
          report_type
        )
      end

      def pass!(approval_rule)
        passed_rules.add(approval_rule)
        return unless approval_rule.scan_result_policy_read

        violations.remove_violation(approval_rule.scan_result_policy_read)
      end

      def fail!(approval_rule, data: nil, context: nil)
        failed_rules.add(approval_rule)
        return unless approval_rule.scan_result_policy_read

        violations.add_violation(approval_rule.scan_result_policy_read, data, context: context)
      end

      def error!(approval_rule, error, **extra_data)
        if excluded?(approval_rule)
          pass!(approval_rule)
          return
        end

        failed_rules.add(approval_rule)
        return unless approval_rule.scan_result_policy_read

        violations.add_error(approval_rule.scan_result_policy_read, error, **extra_data)
      end

      private

      attr_reader :merge_request, :passed_rules, :failed_rules, :approval_rules, :report_type

      delegate :project, to: :merge_request

      def excluded?(rule)
        return true if rule.scan_result_policy_read&.fail_open?
        return false if ::Feature.disabled?(:unblock_rules_using_execution_policies, project.group)
        return false unless rule_excludable?(rule)
        return true if scan_execution_policy_scan_enforced?(rule)

        false
      end

      def scan_execution_policy_scan_enforced?(rule)
        scanners = extract_scanners(rule)
        return false if scanners.blank?

        scanners.all? do |scanner|
          active_scan_execution_policy_scans.include?(scanner)
        end
      end

      # Only allow rules to be excludable if they only target newly detected states. We cannot reliably exclude
      # previously detected states based on active SEP, as they may not had been enforced on the target branch.
      def rule_excludable?(rule)
        case rule.report_type
        when 'scan_finding'
          only_newly_detected?(rule)
        when 'license_scanning'
          rule.scan_result_policy_read&.only_newly_detected_licenses? || false
        else
          false
        end
      end

      def extract_scanners(rule)
        case rule.report_type
        when 'scan_finding'
          rule.scanners.presence || Security::ScanExecutionPolicy::PIPELINE_SCAN_TYPES
        when 'license_scanning'
          %w[dependency_scanning]
        end
      end

      def active_scan_execution_policy_scans
        active_scans_for_ref = project.all_security_orchestration_policy_configurations
        .flat_map do |configuration|
          configuration.active_policies_pipeline_scan_actions_for_project(merge_request.source_branch_ref, project)
        end
        # rubocop:disable Database/AvoidUsingPluckWithoutLimit, CodeReuse/ActiveRecord -- used on hashes
        active_scans_for_ref.pluck(:scan)
        # rubocop:enable Database/AvoidUsingPluckWithoutLimit, CodeReuse/ActiveRecord
      end
      strong_memoize_attr :active_scan_execution_policy_scans

      def violations
        @violations ||= Security::SecurityOrchestrationPolicies::UpdateViolationsService.new(merge_request, report_type)
      end
    end
  end
end
